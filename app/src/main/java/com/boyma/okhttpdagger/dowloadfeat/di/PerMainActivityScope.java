package com.boyma.okhttpdagger.dowloadfeat.di;

import javax.inject.Scope;

@Scope
public @interface PerMainActivityScope {
}
