package com.boyma.okhttpdagger.dowloadfeat.di;

import com.boyma.okhttpdagger.di.NetComponent;
import com.boyma.okhttpdagger.dowloadfeat.IDownloadRepository;

import dagger.Component;

@PerMainActivityScope
@Component(modules = {DownloadModule.class},  dependencies = NetComponent.class)
public interface DownloadComponent {

    IDownloadRepository getIDownloadRepository();
}
