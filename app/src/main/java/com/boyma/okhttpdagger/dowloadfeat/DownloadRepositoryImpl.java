package com.boyma.okhttpdagger.dowloadfeat;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class DownloadRepositoryImpl implements IDownloadRepository {

    private DowloadApi api;


    public DownloadRepositoryImpl(DowloadApi api) {
        this.api = api;
        System.out.println("DownloadRepositoryImpl constructor");
    }

    @Override
    public Single<Response<ResponseBody>> downloadFileWithDynamicUrlAsync(String url) {
        return api.downloadFileWithDynamicUrlAsync(url);
    }
}
