package com.boyma.okhttpdagger.dowloadfeat.di;

import com.boyma.okhttpdagger.dowloadfeat.DowloadApi;
import com.boyma.okhttpdagger.dowloadfeat.DownloadRepositoryImpl;
import com.boyma.okhttpdagger.dowloadfeat.IDownloadRepository;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class DownloadModule {

    @Provides
    @PerMainActivityScope
    public IDownloadRepository provideRepository(DowloadApi api){
        return new DownloadRepositoryImpl(api);
    }

    @Provides
    @PerMainActivityScope
    public DowloadApi provideApi(Retrofit retrofit){
        System.out.println("Api Constructor");
        return retrofit.create(DowloadApi.class);
    }

}
