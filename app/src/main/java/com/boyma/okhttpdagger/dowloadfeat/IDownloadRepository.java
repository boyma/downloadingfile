package com.boyma.okhttpdagger.dowloadfeat;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;

public interface IDownloadRepository {
    Single<Response<ResponseBody>> downloadFileWithDynamicUrlAsync(String url);
}
