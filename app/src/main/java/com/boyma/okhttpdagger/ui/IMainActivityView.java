package com.boyma.okhttpdagger.ui;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface IMainActivityView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void showToast(String s);

    void setText(String s);

    void hideProgressBar();
}
