package com.boyma.okhttpdagger.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.boyma.okhttpdagger.R;

public class MainActivity extends MvpAppCompatActivity implements IMainActivityView {

    private TextView tv;

    @InjectPresenter
    MainActivityPresenter mpresenter;

    @Override
    protected void onPause() {
        super.onPause();
        mpresenter.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();

        mpresenter.onCreate(getExternalCacheDir());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initUI() {
        tv = findViewById(R.id.tv);
    }

    @Override
    public void showToast(String s) {
        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }

    @Override
    public void setText(String s) {
        tv.setText(s);
    }

    @Override
    public void hideProgressBar() {
        findViewById(R.id.progressBar).setVisibility(View.GONE);
    }
}
