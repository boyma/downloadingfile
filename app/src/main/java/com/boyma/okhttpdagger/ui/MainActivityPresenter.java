package com.boyma.okhttpdagger.ui;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.boyma.okhttpdagger.App;
import com.boyma.okhttpdagger.dowloadfeat.di.DaggerDownloadComponent;
import com.boyma.okhttpdagger.dowloadfeat.di.DownloadComponent;

import java.io.File;
import java.io.IOException;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import retrofit2.Response;

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<IMainActivityView>{


    private DownloadComponent downloadComponent;
    private boolean firstlaunch = true;

    private CompositeDisposable subcriber = new CompositeDisposable();
    private Single<File> d;

    public MainActivityPresenter() {
        downloadComponent = DaggerDownloadComponent.builder().netComponent((App.getNetComponent())).build();
    }


    @SuppressLint("CheckResult")
    public void onCreate(File externalCacheDir) {
        d = downloadComponent.getIDownloadRepository().downloadFileWithDynamicUrlAsync("https://r8---sn-pivhx-n8vz.googlevideo.com/videoplayback?ipbits=0&key=yt6&itag=22&mm=31%2C29&txp=4531432&mn=sn-pivhx-n8vz%2Csn-n8v7znze&requiressl=yes&mime=video%2Fmp4&source=youtube&ms=au%2Crdu&mt=1540911557&mv=m&c=WEB&sparams=dur%2Cei%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cnh%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cexpire&ip=109.163.195.226&fvip=11&lmt=1540572276220675&ei=IHLYW9KYKJujyQXenbrICg&id=o-ALMIMiAJQ14pdm8yvk5YrtCFAB6yyqdNcG1YRZYRcYig&initcwndbps=636250&ratebypass=yes&nh=%2CIgpwcjAyLnN2bzA2KgkxMjcuMC4wLjE&dur=887.722&expire=1540933248&pl=24&signature=13602F2F670FAE313C533E8C138BAC25D09F74CB.82AE7C33ACDDF6AAE66D1F2419CB6D4ED12F2D7E&video_id=QkaC23IIbks&title=Porsche+%D0%9A%D0%90%D0%95%D0%9D+%D0%B7%D0%B0+750.000%D1%80.+-+%D0%98%D0%94%D0%95%D0%90%D0%9B%D0%95%D0%9D+%21")
                .flatMap(processResponse(externalCacheDir))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        d.subscribe(this::onDownloadSuccess, this::onDownloadFailed);

    }

    private void onDownloadFailed(Throwable throwable) {
        System.out.println("onError"+throwable);
    }

    private void onDownloadSuccess(File file) {
        System.out.println("onSuccess^"+"File downloaded to " + file.getAbsolutePath());
        getViewState().hideProgressBar();
    }

    private DisposableSingleObserver<File> handleResult() {
        return new DisposableSingleObserver<File>() {
            @Override
            public void onSuccess(File file) {
                System.out.println("onSuccess^"+"File downloaded to " + file.getAbsolutePath());
                getViewState().hideProgressBar();
            }

            @Override
            public void onError(Throwable e) {
                System.out.println("onError"+e);
            }
        };
    }

    private Function<Response<ResponseBody>, Single<File>> processResponse(File externalCacheDir) {
        if (subcriber.isDisposed())return new Function<Response<ResponseBody>, Single<File>>() {
            @Override
            public Single<File> apply(Response<ResponseBody> response) throws Exception {
                return null;
            }
        };
        return responseBodyResponse -> saveToDiskRx(responseBodyResponse,externalCacheDir);
    }

    private Single<File> saveToDiskRx(Response<ResponseBody> response, File externalCacheDir) {

        return Single.create(emitter -> {
            try {
                if (subcriber.isDisposed()) return;

                //String header = response.headers().get("Content-Disposition");
                String filename = "asdasd";

                //new File("/data/data/" + "com.com.com" + "/games").mkdirs();
                File destinationFile = new File(externalCacheDir,filename);

                BufferedSink bufferedSink = Okio.buffer(Okio.sink(destinationFile));


                bufferedSink.writeAll(response.body().source());
                bufferedSink.close();

                emitter.onSuccess(destinationFile);
            } catch (IOException e) {
                e.printStackTrace();
                emitter.onError(e);
            }
        });
    }

    public void onPause() {
        System.out.println("onPause");
        subcriber.clear();
    }
}
